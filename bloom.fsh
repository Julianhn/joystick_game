vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
    vec4 orig = texture2D(texture, texture_coords);
    vec4 sum = vec4(0.0);
    
    for(int x = -5; x <= 5; x++){
	for(int y = -5; y <= 5; y++){
	    sum += texture2D(texture, 
			     texture_coords + 4*vec2(float(x), float(y))/(vec2(1920,1080)));
	}
    }
    sum = sum/(100);
    return 0.6*sum + orig;
}
