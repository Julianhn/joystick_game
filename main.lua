CIRCLE_SEGMENTS = 10

function pack(...)
   return { n = select("#", ...), ... }
end

function love.load()
   jss = love.joystick.getJoysticks()
   js = jss[1]
   if (js) then
      print(js:getName())
   end

   vec = {0,0,0,0,0,0} 
   
   radius = 10
   angle = 0
   
   world = love.physics.newWorld(0,0, true)
   b = love.physics.newBody(world, x, y, "dynamic")
   
   shape_rect = love.physics.newRectangleShape( 0, 0, 100, 200 )
   fixture = love.physics.newFixture( b, shape_rect, 1.0 )
   fixture:setUserData({type="bus",time = math.random()*255})
   
   b:setInertia(100000)

   love.window.setMode(1920, 1080, {fullscreen=true, fullscreentype="desktop", fsaa=16})
   
   shape_block = love.physics.newRectangleShape( 400, 400 )
   shape_block_long = love.physics.newRectangleShape( 400, 800 )
   
   local BLOCK_SPACING = 600
   local CITY_SIZE = 20
   -- city code
   for i=1, CITY_SIZE do
      for j=1, CITY_SIZE do
	 if (j % 2 == 0) then
	    x = i*BLOCK_SPACING - ((CITY_SIZE/2)*BLOCK_SPACING) +BLOCK_SPACING/2
	    y = j*BLOCK_SPACING - ((CITY_SIZE/2)*BLOCK_SPACING)
	 else
	    x = i*BLOCK_SPACING - ((CITY_SIZE/2)*BLOCK_SPACING)
	    y = j*BLOCK_SPACING - ((CITY_SIZE/2)*BLOCK_SPACING)
	 end
	 
	 b_block = love.physics.newBody( world, x, y, "dynamic" )
	 if (math.random()>0.8) then
	    fixture = love.physics.newFixture( b_block, shape_block_long, 5 )
	 else
	    fixture = love.physics.newFixture( b_block, shape_block, 5 )
	 end
	 fixture:setUserData({type="block",time = math.random()*255})
      end
   end

   vel_bus_old = 0
   vel_bus = 0
   
   -- callbacks collision
   world:setCallbacks(
      function(a, b2, c)
      end
      ,
      function(a, b2, c)
      end
      ,
      function(a, b2, c, norm_impulse, tangent_impulse, normal_impulse2, tangent_impulse2)
	 if (not bus_involvement(a, b2)) then
	    return
	 end
	 vib_speed = 0
      end
      ,
      function(a, b2, c, norm_impulse, tangent_impulse, normal_impulse2, tangent_impulse2)
	 if (not bus_involvement(a, b2)) then
	    return
	 end
	 -- collision feels
	 vib_speed = math.min(2000, math.abs(norm_impulse))/2000

	 --print(norm_impulse)
      end
   )

   canvas = love.graphics.newCanvas()
   canvas2 = love.graphics.newCanvas()
   
   bloom_shader = love.graphics.newShader("bloom.fsh")
end

function bus_involvement(a, b)
   return (a:getUserData().type == "bus" or b:getUserData().type == "bus")
end

function love.keypressed(key)
   if key == "w" then
      vec[1] = vec[1] - 1
   end
   if key == "s" then
      vec[1] = vec[1] + 1
   end
   if key == "a" then
      vec[2] = vec[2] - 1
   end
   if key == "d" then
      vec[2] = vec[2] + 1
   end
end

function love.keyreleased(key)
   if key == "w" then
      vec[1] = vec[1] + 1
   end
   if key == "s" then
      vec[1] = vec[1] - 1
   end
   if key == "a" then
      vec[2] = vec[2] + 1
   end
   if key == "d" then
      vec[2] = vec[2] - 1
   end
end

function love.update(dt)
   if (js) then
      axes = pack(js:getAxes())
   else
      axes = vec
   end
   
   
   -- motion
   if (math.abs(axes[1]) > 0.2) or
   (math.abs(axes[2]) > 0.2) then
      force_x, force_y = b:getWorldVector(axes[1]*5000, axes[2]*5000)
      b:applyForce(force_x, force_y)
   end
   if (math.abs(axes[4]) > 0.2) then
      b:applyTorque(axes[4]*50000)
   end

   if js then
      js:setVibration(vib_speed, vib_speed)
   end

   -- box2d
   world:update(dt)
   
   print_debug_stuff()
   
   -- rotate hue values for blocks
   local bodies = world:getBodyList()
   for b=#bodies,1,-1 do
      local body = bodies[b]
      local fixtures = body:getFixtureList()
      for i=1,#fixtures do
	 local fixture = fixtures[i]

	 local data =  fixture:getUserData()
	 if (data.type == "block") then
	    data.time = data.time + dt
	    fixture:setUserData(data)
	 end

	 if (data.type == "bus") then
	    data.time = data.time + dt
	    fixture:setUserData(data)
	 end
      end
   end

end

function print_debug_stuff()
   print("---------")
   for i, v  in ipairs(axes) do
      print(v)
   end

   if js then
      l_vib, r_vib = js:getVibration()
      print("_______")
      print(l_vib..",\t"..r_vib)
   end
end

function love.draw()
   -- canvas stuff :D
   canvas:clear(0,0,0, 20)
   love.graphics.setCanvas(canvas)
   
   love.graphics.push()

   -- camera work
   local x, y = b:getPosition()
   love.graphics.translate(love.graphics.getWidth()/2, love.graphics.getHeight()/2)
   love.graphics.rotate(-b:getAngle())
   love.graphics.translate(-x, -y )
   
   -- world stuff
   debugWorldDraw(world)
   
   love.graphics.pop()

   love.graphics.setCanvas(canvas2)
   love.graphics.setColor(255, 255, 255, 255)
   
   love.graphics.draw(canvas)
   love.graphics.setCanvas()
   love.graphics.setColor(255,255,255)
   -- shaders! :D
   love.graphics.setShader(bloom_shader)
   love.graphics.draw(canvas2)
   -- end shader
   love.graphics.setShader()
end

function debugWorldDraw(world)
   local bodies = world:getBodyList()
   
   for b=#bodies,1,-1 do
      local body = bodies[b]
      local bx,by = body:getPosition()
      local bodyAngle = body:getAngle()
      love.graphics.push()
      love.graphics.translate(bx,by)
      love.graphics.rotate(bodyAngle)
      
      --math.randomseed(42) --for color generation
      
      local fixtures = body:getFixtureList()
      for i=1,#fixtures do
	 local fixture = fixtures[i]
	 local shape = fixture:getShape()
	 local shapeType = shape:getType()
	 local isSensor = fixture:isSensor()
	 
	 if (isSensor) then
	    love.graphics.setColor(0,0,255,255)
	 else
	    love.graphics.setColor(HSV(math.random(0, 255), 255, 255))
	 end
	 
	 local data =  fixture:getUserData()
	 if (data.type == "block") then
	    local hue = fixture:getUserData().time*20
	    love.graphics.setColor(HSV(hue % 255, 255, 255))
	 end

	 if (data.type == "bus") then
	    --local hue = fixture:getUserData().time*100
	    love.graphics.setColor(HSV(0, 0, 255))
	 end
	 
	 love.graphics.setLineWidth(1)
	 if (shapeType == "circle") then
	    local x,y = fixture:getMassData() --0.9.0 missing circleshape:getPoint()
	    --local x,y = shape:getPoint() --0.9.1
	    local radius = shape:getRadius()
	    love.graphics.circle("fill",x,y,radius,15)
	    love.graphics.setColor(0,0,0,255)
	    love.graphics.circle("line",x,y,radius,15)
	    local eyeRadius = radius/4
	    love.graphics.setColor(0,0,0,255)
	    love.graphics.circle("fill",x+radius-eyeRadius,y,eyeRadius,10)
	 elseif (shapeType == "polygon") then
	    if (data.type == "block") then
	       local points = {shape:getPoints()}
	       love.graphics.polygon("fill",points)
	    else
	       local points = {shape:getPoints()}
	       love.graphics.polygon("fill",points)
	       love.graphics.setColor(0,0,0,255)
	       love.graphics.polygon("line",points)
	    end
	 elseif (shapeType == "edge") then
	    love.graphics.setColor(0,0,0,255)
	    love.graphics.line(shape:getPoints())
	 elseif (shapeType == "chain") then
	    love.graphics.setColor(0,0,0,255)
	    love.graphics.line(shape:getPoints())
	 end
      end
      love.graphics.pop()
   end
   
   local joints = world:getJointList()
   for index,joint in pairs(joints) do
      love.graphics.setColor(0,255,0,255)
      local x1,y1,x2,y2 = joint:getAnchors()
      if (x1 and x2) then
	 love.graphics.setLineWidth(3)
	 love.graphics.line(x1,y1,x2,y2)
      else
	 love.graphics.setPointSize(3)
	 if (x1) then
	    love.graphics.point(x1,y1)
	 end
	 if (x2) then
	    love.graphics.point(x2,y2)
	 end
      end
   end
   
   local contacts = world:getContactList()
   for i=1,#contacts do
      love.graphics.setColor(255,0,0,255)
      love.graphics.setPointSize(3)
      local x1,y1,x2,y2 = contacts[i]:getPositions()
      if (x1) then
	 love.graphics.point(x1,y1)
      end
      if (x2) then
	 love.graphics.point(x2,y2)
      end
   end
end

function love.keypressed(key)
   if key == "escape" then
      love.event.push("quit")
   end
end

-- Converts HSV to RGB. (input and output range: 0 - 255)
function HSV(h, s, v)
   if s <= 0 then return v,v,v end
   h, s, v = h/256*6, s/255, v/255
   local c = v*s
   local x = (1-math.abs((h%2)-1))*c
   local m,r,g,b = (v-c), 0,0,0
   if h < 1     then r,g,b = c,x,0
   elseif h < 2 then r,g,b = x,c,0
   elseif h < 3 then r,g,b = 0,c,x
   elseif h < 4 then r,g,b = 0,x,c
   elseif h < 5 then r,g,b = x,0,c
   else              r,g,b = c,0,x
   end return (r+m)*255,(g+m)*255,(b+m)*255
end
